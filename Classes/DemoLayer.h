//
//  DemoLayer.h
//  cchelp-action-factory-test_
//
//  Created by Chau Giang on 5/19/15.
//
//

#ifndef __cchelp_action_factory_test___DemoLayer__
#define __cchelp_action_factory_test___DemoLayer__

#include <cocos2d.h>
#include "hmap.h"

USING_NS_CC;

    class DemoLayer : public Layer
    {
    public:
        virtual bool init();
        
        CREATE_FUNC(DemoLayer);
        static Scene* createScene();
    };


#endif /* defined(__cchelp_action_factory_test___DemoLayer__) */
