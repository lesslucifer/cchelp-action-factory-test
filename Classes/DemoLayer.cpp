//
//  DemoLayer.cpp
//  cchelp-action-factory-test_
//
//  Created by Chau Giang on 5/19/15.
//
//

#include "DemoLayer.h"
#include "MapContainer.h"
#include <vector>

using namespace std;


Scene* DemoLayer::createScene()
{
    Scene* result = Scene::create();
    DemoLayer* layout = DemoLayer::create();
    result->addChild(layout);
    return result;
}

bool DemoLayer::init()
{
    if (!Layer::init())
        return false;
    vector<int> list;
    vector<int*> list1;
    Vector<Node*> list2;
    return true;
}